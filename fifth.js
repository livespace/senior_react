/*
    W reducerze createApp przy obsłudze każdej z akcji w kodzie jest jakiś problem.
    Jak można poprawić każdy z nich? Dlaczego z ich kodem jest coś nie tak?
*/

const initialState = {
    visibilityFilter: 'none',
    products: [
        {
            id: 305,
            price: 100,
            lastSold: '2020-02-12T13:19:15.784Z'
        },
        {
            id: 308,
            price: 500,
            lastSold: '2020-02-14T11:12:18.113Z'
        },
        {
            id: 309,
            price: 299,
            lastSold: '2020-02-14T14:32:06.361Z'
        },
    ]
}

function createApp(state = initialState, action) {
    switch(action.type){
        case SET_VISIBILITY_FILTER:
            Object.assign({}, state, {
                visibilityFilter: action.data.filter
            })
            return state

        case CHANGE_PRODUCT_PRICE:
            const stateCopy = {...state}
            stateCopy.products[action.data.id].price = action.data.newPrice
            return stateCopy

        case UPDATE_LAST_SOLD:
            const stateCopy = {
                ...state,
                products: [...state.products]
            }
            stateCopy.products[action.data.id].lastSold = (new Date()).toISOString()
            return stateCopy

        default:
            return state
    }
}
/*
    Ten komponent nie działa 😱 Jak trzeba go naprawić? Dlaczego nie działał?
    Co w tym kompnencie można zmienić, żeby był ładniej/sensowniej napisany?
*/

import React, { Component } from 'react'

class App extends Component {
    constructor(props) {
        super();
        this.state = {
            name: this.props.name || 'Anonymous'
        }
    }
    render() {
        return (
            <div>
                <input type="text"/>
                <p>Hello {this.state.name}</p>
            </div>
        );
    }
}